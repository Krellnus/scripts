#!/bin/sh

# script to automate the extraction of various archive and compression formats.
file=$(readlink -f "$1")
name="${file%.*}"
ext="${file##*.}"

case "$ext" in
    gz) gzip -d "$file" ;;
    tar) tar -xvf "$file" ;;
esac
