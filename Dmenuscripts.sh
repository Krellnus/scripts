#!/bin/sh

oldnew=$(printf "Modify Existing\nCreate New" | dmenu -i -p "Modify An Existing Script? ")
[ -z "$oldnew" ] && exit 1
case $oldnew in
    "Modify Existing") chosen=$(find ~/bin/scripts ~/bin/statusbar | dmenu -i -p "Which Script Should Be Edited? ") && st -e "$EDITOR" "$chosen" ;;
    "Create New") name=$(echo | dmenu -p "Enter Script Name: ") && st -e "$EDITOR" ~/bin/scripts/"$name".sh && chmod +x ~/bin/scripts/"$name".sh ;;
esac >/dev/null
