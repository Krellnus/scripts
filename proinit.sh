#!/bin/sh

# Script to automate the starting of various projects/programs in various languages 

proname=$(readlink -f "$1")
dir="${proname%.*}"
name="${dir##*/}"
ext="${proname##*.}"

case "$ext" in
    hs) stack new "$name" ; cd "$dir" ; stack setup ; git init ;;
    py) mkdir "$dir" ; cd "$dir" ; touch "$dir/$name.$ext" ; git init ;;
    R) mkdir "$dir" ; cd "$dir" ; cp ~/.vim/templates/skeleton.R "$dir/$name.$ext" ; git init ;;
    sh) mkdir "$dir" ; cd "$dir" ; cp ~/.vim/templates/skeleton.sh "$dir/$name.$ext" ;
        git init ;;
    *) echo "Not a recognised extension.\nMake sure the name has a recognised file extension." && exit ;;
esac
