#!/bin/sh

chosen=$(find ~/AppImages| dmenu -i -l 20)
[ -z "$chosen" ] && exit
exec "$chosen"
