#!/bin/sh

zoom=$(printf "Set Compositor\nReset Compositor" | dmenu -i -l -p)
case $zoom in
    "Set Compositor") killall xcompmgr && xcompmgr -c -l0 -t0 -r0 -o.00 ;;
    "Reset Compositor") killall xcompmgr && xcompmgr -c ;;
esac >/dev/null
