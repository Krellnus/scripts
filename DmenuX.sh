#!/bin/sh

action=$(printf "Suspend\nShutdown\nLogout\nLock Screen\nReboot\nRestart DWM" | dmenu -i -p "What do you want to do? ")
[ -z "$action" ] && exit
case $action in
    "Suspend") i3lock -i ~/Pictures/lock_screen.png && loginctl suspend;;
    "Shutdown") sudo poweroff ;;
    "Logout") kill -TERM "$(pidof -s "$(wmctrl -m | awk '/Name:/{print $2}')")" ;;
    "Lock Screen") i3lock -i ~/Pictures/lock_screen.png ;;
    "Reboot")  sudo reboot ;;
    "Restart DWM") kill -HUP "$(pidof -s "$(wmctrl -m | awk '/Name:/{print $2}')")" ;;
esac >/dev/null
