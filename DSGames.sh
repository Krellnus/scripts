#!/bin/sh

chosen=$(ls ~/bin/DS\ Games | dmenu -i -l 20)
[ "$chosen" != "" ] || exit
desmume ~/bin/DS\ Games/"$chosen"
