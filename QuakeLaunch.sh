#!/bin/sh

chosen=$(echo "Quake 1\nScourge of Armagon\nDissolution of Eternity\nDimension of the Past\nArcane Dimensions" | dmenu -i -l -p)
[ -z "$chosen" ] && exit
case $chosen in
	"Quake 1") exec quakespasm -basedir ~/.quakespasm ;;
	"Scourge of Armagon") exec quakespasm -basedir ~/.quakespasm -hipnotic ;;
	"Dissolution of Eternity") exec quakespasm -basedir ~/.quakespasm -rogue ;;
	"Dimension of the Past") exec quakespasm -basedir ~/.quakespasm -game dopa ;;
	"Arcane Dimensions") exec quakespasm -basedir ~/.quakespasm -game ad ;;
esac >/dev/null
