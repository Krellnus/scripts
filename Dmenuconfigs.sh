#!/bin/sh

chosen=$(ls -AB -d -1 ~/.* ~/.*/* ~/.config/*/* ~/.config/*/*/* | dmenu -i -p "Which Config Should Be Edited? ")
[ -z "$chosen" ] && exit 1
st -e $EDITOR "$chosen"
