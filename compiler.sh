#!/bin/sh

# Script to automate the compilation/building/executing of documents after editing. 

file=$(readlink -f "$1")
name="${file%.*}"
ext="${file##*.}"

case "$ext" in
    h) sudo make clean install ;;
    hs) stack ghc $file ;;
    md) pandoc -f markdown -t pdf $file --pdf-engine=xelatex > $name.pdf ;;
    ms) groff -ms $file -T pdf > $name.pdf ;;
    [rR]md) Rscript -e "rmarkdown::render('$file', quiet=TRUE)" ;;
esac
