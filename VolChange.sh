#!/bin/sh

case "$1" in 
    "up") pulsemixer --change-volume +5;;
    "down") pulsemixer --change-volume -5 ;;
    "toggle") pulsemixer --toggle-mute ;;
esac

kill -s 44 $(pidof dwmblocks) 
