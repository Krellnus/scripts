#!/bin/sh

chosen=$(printf "Docked\nUndocked\nGaming" | dmenu -i)
[ -z "$chosen" ] && exit
case $chosen in
    "Docked") xrandr --output eDP1 --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI1 --primary --mode 1920x1080 --pos 1920x0 --rotate normal --output VIRTUAL1 --off && ~/bin/scripts/remaps.sh ;;
    "Undocked") xrandr --output eDP1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI1 --off --output VIRTUAL1 --off ;;
    "Gaming") xrandr --output eDP1 --off --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off ;;
esac >/dev/null
