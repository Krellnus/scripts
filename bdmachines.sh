#!/bin/sh

case "$1" in
    "up") nmcli connection up --ask SCEM-OpenVPN ;;
    "down") nmcli connection down SCEM-OpenVPN && exit ;;
esac

machine=$(printf "1\n2\n3\n4" | dmenu -i -p "What machine do you want to connect to? ")

ssh sfitzpatrick@bd-client-0"$machine".scem.uws.edu.au
