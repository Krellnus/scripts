#!/bin/sh

fileName="$HOME/Documents/Ideas/src/$(date +%d-%m-%Y).ms"

# [ -z "$fileName" ] && echo ".TL\nNotes for $(date +%d-%m-%Y)" > "$fileName"
if [ ! -f $fileName ]; then
    echo ".TL\nNotes for $(date +%d-%m-%Y)" > $fileName
fi

vim -c "norm Go"\
	-c "norm Go.SH" \
        -c "norm Go$(date +%H:%M)" \
        -c "norm Go.LP" \
	-c "norm Go" \
	-c "norm zz" \
	-c "filetype detect" \
	-c "startinsert" $fileName
