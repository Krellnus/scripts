#!/bin/sh

task=$(echo "Video and Audio\nAudio Only" | dmenu -i -p "What would you like to download? " -l 20)
[ -z "$task" ] && exit
link=$(xclip -o | dmenu -i -p "Copy the video link to download: " -l 20)
[ -z "$link" ] && exit
case $task in 
    "Video and Audio") echo "$link" | youtube-dl -i -a - ;;
    "Audio Only") echo "$link" | youtube-dl -i -x -a - ;;
esac >/dev/null
