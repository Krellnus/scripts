#!/bin/sh

bookpaper=$(printf "A Book\nAn Academic Paper\nA Paper Summary" | dmenu -i -p "What would you like to read? ")
[ -z "$bookpaper" ] && exit 1
case $bookpaper in
    "A Book") chosen=$(ls ~/Documents/Books | dmenu -i -l 20) && zathura ~/Documents/Books/"$chosen" ;;
    "An Academic Paper") chosen=$(ls ~/Documents/University/Papers | dmenu -i -l 20) && zathura ~/Documents/University/Papers/"$chosen" ;;
    "A Paper Summary") chosen=$(ls ~/Documents/University/PhD/papersums/*.pdf | dmenu -i -l 20) && zathura "$chosen" ;;
esac >/dev/null
